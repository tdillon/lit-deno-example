self.addEventListener('load', _ => {
    for (const myElement of document.querySelectorAll<Element & { name: string }>('.load')) {
        myElement.name = '3️⃣ SUCCESS - load event'
    }
})

self.addEventListener('mousemove', (e: MouseEvent) => {
    for (const myElement of document.querySelectorAll<Element & { name: string }>('.mouse')) {
        myElement.name = `4️⃣ SUCCESS - mousemove event - ${e.x},${e.y}`
    }
})
