var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/// <reference lib="dom" />
import { html, css, LitElement } from 'https://esm.sh/lit@3.0.1';
import { customElement, property } from 'https://esm.sh/lit@3.0.1/decorators';
let SimpleLit = class SimpleLit extends LitElement {
    constructor() {
        super(...arguments);
        this.name = '1️⃣ default property value';
    }
    static { this.styles = css `p { color: #09c }`; }
    render() {
        return html `<p>${this.name}</p>`;
    }
};
__decorate([
    property(),
    __metadata("design:type", Object)
], SimpleLit.prototype, "name", void 0);
SimpleLit = __decorate([
    customElement('simple-lit-vanilla')
], SimpleLit);
export { SimpleLit };
